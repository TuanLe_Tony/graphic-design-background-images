
# IMAGES



# HAND DRAWING
## Tiger
![Scheme](HandDrawing/tigerhanddraw.jpeg)
## Wolf
![Scheme](HandDrawing/handdraw.jpeg)
## I Love Everyone
![Scheme](HandDrawing/iloveyou.jpeg)
After registed you able to login

# 3Ds Max Render

## Store Image 1
![Scheme](3Ds-Max-RenderImage/PaintsStore1.jpeg)
## Store Image 2
![Scheme](3Ds-Max-RenderImage/PaintsStore.jpeg)
## HOUSE
![Scheme](3Ds-Max-RenderImage/House.jpeg)
## BED ROOM
![Scheme](3Ds-Max-RenderImage/3DsMaxBedRoom.png)
## BED ROOM TOP
![Scheme](3Ds-Max-RenderImage/3DsMaxTopRenderBedRoom.png)
## GLASS
![Scheme](3Ds-Max-RenderImage/3DsMaxRenderGlass.png)

# Photoshop Images

## Fight Tiger
![Scheme](PhotoShop/Photoshop1.png)
## Tiger
![Scheme](PhotoShop/Photoshop2.png)

# CorelDraw

## Coffee
![Scheme](CorelDraw/CorelDrawCoffee.png)
## Restaurant
![Scheme](CorelDraw/CorelDrawRetaurant.png)

# Adobe illustrator

![Scheme](AI/Steel.jpg)








